#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdlib.h>
#include "uart.h"
#include "global.h"
#include "LEDcontroller.h"

#define wait 25
#define NUM_COLORS 3
#define NUM_PIXELS 14
#define NEO_PORT PORTA
#define NEO_PIN 3
#define MAXBRIGHT 250

#define NEODDR  DDRA
#define NEOPORT PORTA
#define NEOMSK  1 << NEO_PIN


uint16_t _NumPix;
uint8_t _neoport;
uint8_t _neopin;
uint8_t _brightness;
uint8_t _pixels[NUM_PIXELS*NUM_COLORS]; //Holds LED color values, 3 bytes each

void LED_init(void)
{    
    _NumPix = NUM_PIXELS;
    _neoport = NEO_PORT;
    _neopin = NEO_PIN;
    _brightness = 0;
    
    NEODDR |= NEOMSK;
    NEOPORT &= ~NEOMSK;
}

void showLED(void)
{    
    cli();
    volatile uint16_t i = _NumPix*3;
    volatile uint8_t *ptr = _pixels;
    volatile uint8_t b = *ptr++;
    volatile uint8_t hi = NEOPORT | NEOMSK;
    volatile uint8_t lo = NEOPORT & ~NEOMSK;
    volatile uint8_t next = lo;
    volatile uint8_t bit = 8;
    
    /* With 16MHz, a clock tick is 62.5ns. The (T = x) comments indicate
     * how many ticks have passed.
     *
     * The code starts with pin set to hi. Then "skip if bit in register cleared"
     * Basically, jump the PC 2 if the 8th bit in the current byte is one. 
     * This skips the next = hi line and outputs lo from the last time next was set.
     * Then, the bit counter is decremented, and the port is written to the next value
     * next is set to lo either way.
     *
     * If the bit counter has hit zero following the last decrement, it will jump
     * to 2:, where it resets bit counter to the value 8, loads the next byte from memory
     * sets the port to lo, waits for a moment, decrements the byte counter,
     * and if the byte counter is not zero, return to 1: and begin outputting the
     * next byte.
     *
     * If the bit counter is not equal to 0, instead of jumping to 2:, it rolls the
     * byte value left by 1, waits three ticks, sets the port to lo, waits three more ticks,
     * then jumps back to the top again, sets the port hi, and begins again.
     *
     * The crucial timing and byte high/low happens primarily in the T=4 to T=8 lines.
     * Based on the bit of the byte after being rolled, the next output bit will be high
     * or low.
     */
     
    
    asm volatile(
    "1:"                      "\n\t"  // Clk  Pseudocode    (T =  0)
    "st   %a[port],  %[hi]"    "\n\t" // 2    PORT = hi     (T =  2)
    "sbrc %[byte],  7"         "\n\t" // 1-2  if(b & 128)
    "mov  %[next], %[hi]"      "\n\t" // 0-1   next = hi    (T =  4)
    "dec  %[bit]"              "\n\t" // 1    bit--         (T =  5)
    "st   %a[port],  %[next]"  "\n\t" // 2    PORT = next   (T =  7)
    "mov  %[next] ,  %[lo]"    "\n\t" // 1    next = lo     (T =  8)
    "breq 2f"          "\n\t"         // 1-2  if(bit == 0) (from dec above)
    "rol  %[byte]"             "\n\t" // 1    b <<= 1       (T = 10)
    "rjmp .+0"                 "\n\t" // 2    nop nop       (T = 12)
    "nop"                      "\n\t" // 1    nop           (T = 13)
    "st   %a[port],  %[lo]"    "\n\t" // 2    PORT = lo     (T = 15)
    "nop"                      "\n\t" // 1    nop           (T = 16)
    "rjmp .+0"                 "\n\t" // 2    nop nop       (T = 18)
    "rjmp 1b"              "\n\t"     // 2    -> head20 (next bit out)
    "2:"                  "\n\t"      //                    (T = 10)
    "ldi  %[bit]  ,  8"        "\n\t" // 1    bit = 8       (T = 11)
    "ld   %[byte] ,  %a[ptr]+" "\n\t" // 2    b = *ptr++    (T = 13)
    "st   %a[port], %[lo]"     "\n\t" // 2    PORT = lo     (T = 15)
    "nop"                      "\n\t" // 1    nop           (T = 16)
    "sbiw %[count], 1"         "\n\t" // 2    i--           (T = 18)
    "brne 1b"              "\n"       // 2    if(i != 0) -> (next byte)
    :[byte]  "+r" (b),
    [bit]   "+r" (bit),
    [next]  "+r" (next),
    [count] "+w" (i)
    :
    [port]  "e"  (&NEOPORT),
    [ptr]    "e" (ptr),
    [hi]     "r" (hi),
    [lo]     "r" (lo)
    );
    
    sei();
    _delay_us(50);
}

void setPixelColor(uint16_t pixel, uint32_t color)
{    
    uint8_t red = color >> 16;
    uint8_t green = color >> 8;
    uint8_t blue = color >> 0;
    
    if(_brightness)
    {
        red = (uint8_t)(((uint16_t)red * (uint16_t)_brightness) >> 8);
        green = (green * _brightness) >> 8;
        blue = (blue * _brightness) >> 8;   
    }
    uint8_t *p = _pixels + pixel*NUM_COLORS;
    *p++ = green;
    *p++ = red;
    *p = blue;
}

void setBrightness(uint8_t brightness)
{    
    uint8_t newBrightness = brightness + 1;
    if(newBrightness != _brightness)
    {
        uint8_t color;
        uint8_t *ptr = _pixels;
        uint8_t oldBrightness = _brightness - 1;
        uint16_t scale;
        
        if(oldBrightness == 0)
        {
            scale = 0;
        }
        else if(brightness == 255)
        {
            scale = 65535 / oldBrightness;
        }
        else
        {
            scale = (((uint16_t)newBrightness << 8) - 1) / oldBrightness;
        }
        
        for(uint16_t i = 0; i < _NumPix*NUM_COLORS; i++)
        {
            color = *ptr;
            *ptr++ = (color * scale) >> 8;
        }
        _brightness = newBrightness;
    }
}

uint8_t* getPixels()
{
    return _pixels;
}

uint16_t numPixels()
{
    return _NumPix;
}

LED_Color_t getPixelInfo(uint16_t pixel)
{
    LED_Color_t color;
    
    uint8_t *p = &_pixels[pixel*NUM_COLORS];
    uint8_t green = *p++;
    uint8_t red = *p++;
    uint8_t blue = *p++;
    
    color.red = red;
    color.green = green;
    color.blue = blue;
    
    return color;
}

void pulseup(int jump)
{
    int u;
    for(u = 1 ; u <= MAXBRIGHT; u += jump)
    {
        setBrightness(u);
        showLED();
    }
}

void pulsedown(int jump)
{
    int i;
    for(i = MAXBRIGHT; i >= 1; i -= jump)
    {
        setBrightness(i);
        showLED();
    }
}

void colorWalk(uint32_t color)
{
    uint16_t u;
    for(u = 0; u < numPixels(); u++)
    {
        uint16_t i;
        for(i = 0; i < numPixels(); i++)
        {
            setPixelColor(i, color*(i==u));
        }
        showLED();
        _delay_ms(wait);
    }
}

void colorWipe(uint32_t color)
{
    uprintf("colorWipe has been entered.\r\n");
    uint16_t i;
    for(i = 0; i < numPixels(); i++)
    {
        setPixelColor(i, color);
        showLED();
        _delay_ms(wait);
    }
}            