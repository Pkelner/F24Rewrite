/*
 * CANControl.c
 *
 * Created: 4/23/2016 6:09:46 PM
 *  Author: Phillip Kelner
 */ 

#include "CANControl.h"
#include <string.h>

uint8_t Buffer[CAN_NUM_MSGS][CAN_MSG_SIZE]; //The buffers where the CAN Data is stored
st_cmd_t Msg[CAN_NUM_MSGS];                 // The CAN mailboxes

void CANBUS_Init(void)
{
    for(uint8_t i = 0; i<CAN_NUM_MSGS; i++)
        Msg[i].pt_data = Buffer[i];
}

void CAN_Reset(void)
{
    can_init((U8) CAN_BAUDRATE);
}

void ClearCANBuffer(CAN_MSGID_t msgID)
{
    memset(Buffer[msgID], 0, CAN_MSG_SIZE * sizeof(Buffer[msgID][0]));
}    

void InitCANRx (CAN_MSGID_t msgID)
{
   // --- RX Command
   Msg[msgID].cmd = CMD_RX;
   
   // --- Wait until RX mode is initiated
   while (can_cmd(&Msg[msgID]) != CAN_CMD_ACCEPTED);
}

void InitCANRxMasked(CAN_MSGID_t msgID, uint16_t mask)
{
    // --- Rx Command
    Msg[msgID].cmd		= CMD_RX; //_DATA_MASKED;
    Msg[msgID].id.std	= mask;

    // --- Wait until RX mode is initiated
    while (can_cmd(&Msg[msgID]) != CAN_CMD_ACCEPTED);
}


uint8_t getCANStatus(CAN_MSGID_t msgID)
{
    return can_get_status(&Msg[msgID]);
}

const st_cmd_t* getCANMessage(CAN_MSGID_t msgID)
{
    return &Msg[msgID];
}

void InitCANTx(CAN_MSGID_t msgID, const uint16_t id, const uint8_t sendBuf[])
{
    memcpy(Buffer[msgID], sendBuf, CAN_MSG_SIZE * sizeof(Buffer[msgID][0]));
    
    // --- Tx Command
    Msg[msgID].id.std   = id;
    Msg[msgID].dlc      = CAN_MSG_SIZE;
    Msg[msgID].cmd      = CMD_TX_DATA;
    
    // --- Wait until Tx is accepted
    while (can_cmd(&Msg[msgID]) != CAN_CMD_ACCEPTED);
}

void WaitForCANCompletion(CAN_MSGID_t msgID)
{
    // --- Wait until command completed
    while (can_get_status(&Msg[msgID]) == CAN_STATUS_NOT_COMPLETED);
}

const uint8_t* getCANBuffer(CAN_MSGID_t msgID)
{
    return Buffer[msgID];
}