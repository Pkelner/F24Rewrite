

#include <compiler.h>

#include "uart.h"

//------------------------------------------------------------------------------
// Print routines for the UART.
//
// Detection of LF ('\n') automatically inserts CR ('\r') into stream.
// Note that this does not change the returned count of characters
// printed.
//
// This groks '%d', '%u', '%x' and '%s'.
//------------------------------------------------------------------------------

//
//  Emit a number to the UART.  Returns the number of characters printed
//  (or that would be printed), or UART_RECEIVER_ERROR (EOF) if we get an error
//  from the UART (which will actually never happen).
//

uint8_t UART_vprintstr(const char* s, va_list ap);

static
uint8_t
PutNumber(
    unsigned int n,                                                     // number to emit
    int radix,                                                          // radix (typically 8, 10, 16)
    int upper,                                                          // nonzero: print uppercase letters
    int width,                                                          // zero, or field with to pad to
    int zeroFill,                                                       // if padding, nonzero=pad with '0'
    int actuallyPrint                                                   // suppress printing
    );

static
uint8_t
PutNumber(
    unsigned int n,                                                     // number to emit
    int radix,                                                          // radix (typically 8, 10, 16)
    int upper,                                                          // nonzero: print uppercase letters
    int width,                                                          // zero, or field with to pad to
    int zeroFill,                                                       // if padding, nonzero=pad with '0'
    int actuallyPrint                                                   // suppress printing
    )
{
    int used = 0;
    char tmpBuf[20];
    int tmpBufIndex = 0;

    //
    //  generate chars of number, in reverse order
    //
    
    while ( tmpBufIndex < sizeof(tmpBuf) )
    {
        if ( actuallyPrint )
        {
            int c = n % radix;

            if ( c < 10 )
            {
                c += '0';
            }
            else if ( upper )
            {
                c += 'A' - 10;
            }
            else
            {
                c += 'a' - 10;
            }

            tmpBuf[tmpBufIndex++] = c;
        }

        ++used;

        if ( ! (n /= radix) )
            break;
    }

    //
    //  fill to field width (with spaces or zeros)
    //

    if ( width > 0 )
    {
        while ( width-- > tmpBufIndex )
        {
            if ( actuallyPrint )
                if (!UART_putbyte(zeroFill ? '0' : ' '))
                    goto Eof;

            ++used;
        }
    }

    //
    //  stuff chars out, reversing so everything comes out in the end
    //

    if ( actuallyPrint )
    {
        while ( tmpBufIndex )
            if (!UART_putbyte(tmpBuf[--tmpBufIndex]))
                goto Eof;

        //  keep track of unused negative padding
        if ( width < 0 )
            ++width;
    }

    //
    //  trailing padding
    //
    
    while ( width++ < 0 )
    {
        if (!UART_putbyte(' '))
            goto Eof;
    }

    return used;

Eof:
    return 0;
}


/* ---------------------------------------------------------------- */


uint8_t UART_vprintstr(const char* s, va_list ap)
{
    int count = 0;

    if ( NULL == s )
        s = "<NULL>";

    while (*s != '\0')
    {
        /*
        if (*s == '\n')
        {
            // Newline detected - add carriage return
            if (UART_putchar('\r') == UART_RECEIVER_ERROR)
            {
                goto Exit;
            }
        }
        */

        if (*s == '%')
        {
            char key;
            int zeroFill = 0;
            int fieldWidth = 0;
            int alignment = 1;

            ++s;

            //  grok leading zero
            if ( *s == '0' )
                zeroFill = 1;

            //  grok leading '-'
            if ( *s == '-' )
            {
                alignment = -1;
                ++s;
            }

            //  grok field width
            while ( *s >= '0' && *s <= '9' )
            {
                fieldWidth = (fieldWidth * 10) + (*s++ - '0');
            }

            fieldWidth *= alignment;        // field width possibly negative

            key = *s++;
            switch ( key )
            {
            case 's':
            {
                const char* s;

                s = va_arg(ap, const char*);

                //xxx handle leading width

                if ( NULL == s )
                    s = "<null>";

                while ( *s )
                {
                    /*
                    if ( *s == '\n' )
                        if ( UART_putchar('\r') == UART_RECEIVER_ERROR )
                            goto Exit;
                    */
                    if (!UART_putbyte(*s++))
                        goto Exit;
                    ++count;
                }

                //xxx handle trailing width
            }
            break;

            //
            //  signed, unsigned decimal
            //
            case 'u':
            case 'd':
            {
                int n;
                bool np;


                n = va_arg(ap, int);

                if ( (key == 'd')
                    && n < 0 )
                {
                    if (!UART_putbyte('-'))
                        goto Exit;
                    n = -n;
                    ++count;
                }

                np = PutNumber(n, 10, 0, fieldWidth, zeroFill, 1);
                
                if ( !np )
                    goto Exit;
                
                count += np;
            }
            break;

            //
            //  hex
            //
            case 'X':
            case 'x':
            {
                unsigned int n;
                bool np;

                n = va_arg(ap, unsigned int);
                np = PutNumber(n, 16, (key == 'D'), fieldWidth, zeroFill, 1);

                if ( !np )
                    goto Exit;
                
                count += np;
            }
            break;

            case 'C':
            case 'c':
            {
                char c = 0;

                c = va_arg(ap, int);
                UART_putbyte(c);
                ++count;
            }
            break;

            default:
                goto Exit;
            }
        }
        else
        {
            if (!UART_putbyte(*(s++)))
            {
                goto Exit;
            }
            count++;
        }
    }

Exit:
    return count;
}


/* ---------------------------------------------------------------- */


int uprintf(const char* s, ...)
{
    va_list ap;
    int count;

    va_start(ap, s);
    count = UART_vprintstr(s, ap);
    va_end(ap);

    return count;
}

