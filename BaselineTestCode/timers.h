/*
 * timers.h
 *
 * Created: 4/20/2016 7:20:03 PM
 *  Author: Phillip Kelner
 */ 


#ifndef TIMERS_H_
#define TIMERS_H_

void timer_init(int delay);

void init_interrupts(void);

void Timer1_ISR(void);

uint16_t getSeconds(void);

int getTimerFlag(void);

void resetTimerFlag(void);

#endif /* TIMERS_H_ */