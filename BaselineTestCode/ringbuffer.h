#pragma once

#ifndef _INC_RINGBUFFER
#define _INC_RINGBUFFER

#include <compiler.h>

#ifdef __cplusplus
extern "C" {
    #endif

    /*
     *  Doubly linked list structure.  Can be used as either a list head, or
     *  as link words.
     */
    
    typedef struct _LIST_ENTRY {
        struct _LIST_ENTRY *Flink;
        struct _LIST_ENTRY *Blink;
    } LIST_ENTRY, *PLIST_ENTRY;
    
    
    /*
     *Ring Buffer structure.
     */
    typedef struct _RING_BUFFER
    {
        uint8_t Bytes;
        uint8_t* Buffer;
        
        
        /* Offset within Buffer to next Read */
        volatile uint8_t ReadOffset;
        
        /* Offset within Buffer to next Write */
        volatile uint8_t WriteOffset;
    } RING_BUFFER, *PRING_BUFFER;


    bool
    InitializeRingBuffer(
        PRING_BUFFER RingBuffer,
        uint8_t* Buffer,
        uint8_t Bytes
        );

    uint8_t
    BytesReadyRingBuffer(
        PRING_BUFFER RingBuffer
        );


    uint8_t
    BytesFreeRingBuffer(
        PRING_BUFFER RingBuffer
        );


    bool
    WriteRingBuffer(
        PRING_BUFFER RingBuffer,
        uint8_t* Buffer,
        uint8_t Bytes
        );


    bool
    InterlockedWriteRingBuffer(
        PRING_BUFFER RingBuffer,
        uint8_t* Buffer,
        uint8_t Bytes
        );


    bool
    ReadRingBuffer(
        PRING_BUFFER RingBuffer,
        uint8_t* Buffer,
        uint8_t Bytes
        );


    bool
    InterlockedReadRingBuffer(
        PRING_BUFFER RingBuffer,
        uint8_t* Buffer,
        uint8_t Bytes
        );

    bool
    PeekRingBuffer(
        PRING_BUFFER RingBuffer,
        uint8_t* Buffer,
        uint8_t Bytes
        );


    bool
    InterlockedPeekRingBuffer(
        PRING_BUFFER RingBuffer,
        uint8_t* Buffer,
        uint8_t Bytes
        );


    #ifdef __cplusplus
}
#endif

#endif