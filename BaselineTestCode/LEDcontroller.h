#ifndef LEDController_H_
#define LEDController_H_

typedef enum Color_enum
{
    RED = 0xFF0000,
    BLUE = 0x0000FF,
    GREEN = 0x00FF00,
    YELLOW = 0xFFFF00,
    PURPLE = 0x551A8B,
    WHITE = 0xFFFFFF,
    NONE = 0x000000
} Color_t;

typedef struct LED_Color {
    uint8_t brightness;
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} LED_Color_t;

void LED_init(void);

void showLED(void);

void setPixelColor(uint16_t pixel, uint32_t color);
    
void setBrightness(uint8_t brightness);

uint8_t* getPixels();

uint16_t numPixels();

LED_Color_t getPixelInfo(uint16_t pixel);
    
void pulseup(int jump);
    
void pulsedown(int jump);
    
void colorWalk(uint32_t color);
    
void colorWipe(uint32_t color);

#endif /* LEDController_H_ */