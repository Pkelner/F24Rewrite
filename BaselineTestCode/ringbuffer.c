#include <compiler.h>
#include "ringbuffer.h"

#include <string.h>

#define RingBuffer_Lock cpu_irq_disable()
#define RingBuffer_Unlock cpu_irq_enable()

bool
RingBufferIsLocked(void);


bool
RingBufferIsLocked(void)
{
    return !cpu_irq_is_enabled();
}

bool
InitializeRingBuffer(
    PRING_BUFFER RingBuffer,
    uint8_t* Buffer,
    uint8_t Bytes
    )
{
    // Ensure pointers valid, Bytes > 0 and Power-of-Two
    Assert(RingBuffer != NULL && Buffer != NULL && Bytes > 0);

    RingBuffer->Buffer = Buffer;
    RingBuffer->Bytes = Bytes;
    RingBuffer->ReadOffset = 0;
    RingBuffer->WriteOffset = 0;

    return true;
}

uint8_t
BytesReadyRingBuffer(
    PRING_BUFFER RingBuffer
    )
{
    // Ensure pointer valid
    Assert(RingBuffer != NULL);

    return (RingBuffer->WriteOffset - RingBuffer->ReadOffset);
}

uint8_t
BytesFreeRingBuffer(
    PRING_BUFFER RingBuffer
    )
{
    // Ensure pointer valid
    Assert(RingBuffer != NULL);

    return (RingBuffer->Bytes - BytesReadyRingBuffer(RingBuffer));
}

//
//  ASSUMES RingBuffer_IsLocked !!!
//
bool
WriteRingBuffer(
    PRING_BUFFER RingBuffer,
    uint8_t* Buffer,
    uint8_t Bytes
    )
{
    uint8_t WriteOffset, WriteOffsetMod;
    uint8_t BytesFree, Bytes0;
    uint8_t* Buffer0;

    Assert(RingBuffer != NULL && Bytes > 0);
    Assert(RingBufferIsLocked(RingBuffer));

    WriteOffset = RingBuffer->WriteOffset;
    BytesFree = BytesFreeRingBuffer(RingBuffer);

    if (Bytes > BytesFree)
    {
        return false;
    }

    WriteOffsetMod = (WriteOffset & (RingBuffer->Bytes - 1)); // Number of bytes into the ring buffer

    Buffer0 = RingBuffer->Buffer + WriteOffsetMod; // Calculates the offset index into the ring buffer

    WriteOffset += Bytes; // Calculate the new write offset after writing the bytes

    Bytes0 = MIN(Bytes, RingBuffer->Bytes - WriteOffsetMod); // The number of bytes until the end of the buffer, before it needs to wrap around

    Bytes -= Bytes0; // calculate the remaining bytes to wrap around

    //
    //  Copy Data to RingBuffer
    //
    if (Buffer != NULL)
    {
        memcpy(Buffer0, Buffer, Bytes0);

        if (Bytes > 0)
        {
            memcpy(RingBuffer->Buffer, Buffer + Bytes0, Bytes);
        }
    }


    RingBuffer->WriteOffset = WriteOffset;

    return true;
}

bool
InterlockedWriteRingBuffer(
    PRING_BUFFER RingBuffer,
    uint8_t* Buffer,
    uint8_t Bytes
    )
{
    Assert(RingBuffer != NULL && Bytes > 0);
    bool success = true;
    RingBuffer_Lock;
    {
        success = WriteRingBuffer(RingBuffer, Buffer, Bytes);
    }
    RingBuffer_Unlock;

    return success;
}



//
//  ASSUMES RingBuffer_IsLocked !!!
//
bool
ReadRingBuffer(
    PRING_BUFFER    RingBuffer,
    uint8_t* Buffer,
    uint8_t Bytes
    )
{
    uint8_t ReadOffset, ReadOffsetMod, WriteOffset;
    uint8_t BytesReady, Bytes0, Bytes1;
    uint8_t* Buffer0;

    Assert(RingBuffer != NULL && Bytes > 0);
    Assert(RingBufferIsLocked(RingBuffer));

    WriteOffset = RingBuffer->WriteOffset;
    ReadOffset = RingBuffer->ReadOffset;
    BytesReady = WriteOffset - ReadOffset;

    if (Bytes > BytesReady)
    {
        return false;
    }
    
    //
    //  Copy Data from RingBuffer
    //
    if (Buffer != NULL)
    {
        ReadOffsetMod = (ReadOffset & (RingBuffer->Bytes - 1));

        Buffer0 = RingBuffer->Buffer + ReadOffsetMod;

        Bytes0 = MIN(Bytes, RingBuffer->Bytes - ReadOffsetMod);

        memcpy(Buffer, Buffer0, Bytes0);

        Bytes1 = Bytes - Bytes0;

        if (Bytes1 > 0)
        {
            memcpy(Buffer + Bytes0, RingBuffer->Buffer, Bytes1);
        }
    }

    ReadOffset += Bytes;

    RingBuffer->ReadOffset = ReadOffset;

    return true;
}



bool
InterlockedReadRingBuffer(
    PRING_BUFFER RingBuffer,
    uint8_t* Buffer,
    uint8_t Bytes
    )
{
    Assert(RingBuffer != NULL && Bytes > 0);
    bool success = true;

    RingBuffer_Lock;
    {
        success = ReadRingBuffer(RingBuffer, Buffer, Bytes);
    }
    RingBuffer_Unlock;

    return success;
}


//
//  ASSUMES RingBuffer_IsLocked !!!
//
bool
PeekRingBuffer(
    PRING_BUFFER RingBuffer,
    uint8_t* Buffer,
    uint8_t Bytes
    )
{
    uint8_t ReadOffset, ReadOffsetMod, WriteOffset;
    uint8_t BytesReady, Bytes0, Bytes1;
    uint8_t* Buffer0;

    Assert(RingBuffer != NULL && Bytes > 0);
    Assert(RingBufferIsLocked(RingBuffer));

    WriteOffset = RingBuffer->WriteOffset;
    ReadOffset = RingBuffer->ReadOffset;
    BytesReady = WriteOffset - ReadOffset;

    if (Bytes > BytesReady)
    {
        return false;
    }

    //
    //  Copy Data from RingBuffer
    //
    if (Buffer != NULL)
    {
        ReadOffsetMod = (ReadOffset & (RingBuffer->Bytes - 1));

        Buffer0 = RingBuffer->Buffer + ReadOffsetMod;

        Bytes0 = MIN(Bytes, RingBuffer->Bytes - ReadOffsetMod);

        memcpy(Buffer, Buffer0, Bytes0);

        Bytes1 = Bytes - Bytes0;

        if (Bytes1 > 0)
        {
            memcpy(Buffer + Bytes0, RingBuffer->Buffer, Bytes1);
        }
    }

    return true;
}


bool
InterlockedPeekRingBuffer(
    PRING_BUFFER RingBuffer,
    uint8_t* Buffer,
    uint8_t Bytes
    )
{
    Assert(RingBuffer != NULL && Bytes > 0);
    
    bool success = true;
    RingBuffer_Lock;
    {
        success = PeekRingBuffer(RingBuffer, Buffer, Bytes);
    }
    RingBuffer_Unlock;

    return success;
}