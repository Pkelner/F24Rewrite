/*
 * CANControl.h
 *
 * Created: 4/23/2016 5:43:19 PM
 *  Author: Phillip Kelner
 */ 


#ifndef CANCONTROL_H_
#define CANCONTROL_H_

#include "config.h"
#include "can_lib.h"

#include <compiler.h>
#include <stdint.h>

#define CAN_DDR     DDRD
#define CAN_PORT    PORTD
#define CAN_PIN     PIND
#define CAN_TX_MASK 0x20
#define CAN_RX_MASK 0x40

#define CAN_MSG_SIZE 8

typedef enum CAN_MSGID
{
    RX1,
    RX2,
    RX3,
    TX1,
    CAN_NUM_MSGS
} CAN_MSGID_t;

void CANBUS_Init(void);

/**
* Performs a Reset on the entire CAN Controller
*/
void CAN_Reset(void);

/**
 * Clears the Message Buffer
 * \param msgID the "mailbox" to clear
 */
void ClearCANBuffer(CAN_MSGID_t msgID);

/**
 * Initializes a receive command and waits until the cmd is accepted
 * @param msgID the "mailbox" to use
 * *param mask the id of the incoming message
 */
void InitCANRx(CAN_MSGID_t msgID);

/**
 * Initializes a masked receive command and waits until command is accepted.
 * @param msgID the "mailbox" to use
 * @param mask the id of the incoming message
 */
void InitCANRxMasked(CAN_MSGID_t msgID, uint16_t mask);

/**
 * Returns the status of the CAN bus
 * \param msgID the "mailbox" to get the status of.
 * \note A call to this function after the transaction is complete
 *		may return Error
 */
uint8_t getCANStatus(CAN_MSGID_t msgID);

/**
 * returns a reference to the entire CAN Message
 * @param msgID the "mailbox" to get the message from
 */
const st_cmd_t* getCANMessage(CAN_MSGID_t msgID);

/**
 * Returns read only a pointer to the message buffer
 * @param msgID the "mailbox" to get the buffer from
 */
const uint8_t * getCANBuffer(CAN_MSGID_t msgID);

/**
 * Initializes the CAN for the Wheel to Transmit a message.
 * @param msgID the "mailbox" to use
 * @param id The CAN message ID
 * @param sendBuf the 8 byte buffer to send over the CAN network
 */
void InitCANTx(CAN_MSGID_t msgID, const uint16_t id, const uint8_t sendBuf[]);

/**
 * Waits until the Current CAN transaction is complete
 * @param msgID the "mailbox" to wait for
 */
void WaitForCompletion(CAN_MSGID_t msgID);

#endif /* CANCONTROL_H_ */