/*
 * BaselineTestCode.c
 *
 * Created: 4/8/2016 8:36:32 PM
 * Author : Phillip Kelner
 * Purpose: Testing Dashboard to drive DownShift pin high, just to confirm
 *          functionality.
 */ 

// TODO Add debug ifdef statements

#include <avr/io.h>
#include <compiler.h>
#include "util/delay.h"
#include "../../../../../toolchain/avr8/avr8-gnu-toolchain/avr/include/avr/iocanxx.h"
#include "ringbuffer.h"
#include "uart.h"
#include <avr/interrupt.h>

#include "string.h"
#include "global.h"
#include "timers.h"
#include "LEDcontroller.h"
#include "CANControl.h"

#define FALSE 0
#define TRUE 1

#define MOTEC_DATA_ID	1520
#define MOTEC_ERROR_ID	1521
#define EDL_DATA_ID		1590
#define DASH_ID			1600

#ifdef DEBUG
#define DEVKIT_ID       1620
#endif

int main(void)
{
    UART_Initialize(9600);
    
    uint8_t CANoutputs[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    CAN_Reset();
    CANBUS_Init();
    
    InitCANRx(RX1);
    InitCANTx(TX1, DEVKIT_ID, CANoutputs);
    
    init_interrupts();
    
    LED_init();
    setBrightness(50);
    
    Color_t colors[7] = {RED, BLUE, GREEN, YELLOW, PURPLE, WHITE, NONE};
    
    colorWipe(RED);
    
    while (1) 
    {
        
    }
}

