/*
 * config.h
 *
 * Created: 2/19/2015 4:55:44 PM
 *  Author: MarkBook
 */


#ifndef CONFIG_H_
#define CONFIG_H_

#include "compiler.h"
#include "at90can_drv.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//******************************************************************************
//***                            Misc Setup                                  ***
//******************************************************************************
#define FOSC (F_CPU/1000)
//*****************************************************************************
//***                           CAN Library Setup                           ***
//*****************************************************************************
#define	SUPPORT_EXTENDED_CANID	1
#define	SUPPORT_TIMESTAMPS		0
#define CAN_BAUDRATE 1000U

//*****************************************************************************
//***                           PWM Library Setup                           ***
//*****************************************************************************
#define USE_TIMER16 TIMER16_1



#endif /* CONFIG_H_ */
