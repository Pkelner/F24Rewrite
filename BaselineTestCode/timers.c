/*
 * timers.c
 *
 * Created: 4/20/2016 7:20:44 PM
 *  Author: Phillip Kelner
 */ 

#include <avr/interrupt.h>
#include <compiler.h>
#include "../../../../../toolchain/avr8/avr8-gnu-toolchain/avr/include/avr/iocanxx.h"

volatile uint16_t seconds = 0;
volatile int SW_TIMER_FLAG = 0;

void timer_init(int delay)
{
    TIMSK1 |= 1<<OCIE1A;
    
    TCNT1 = 0x0000;
    OCR1A = delay;
    
    TCCR1A |= (1 << WGM11) | (1 << WGM10);
    TCCR1B |= (1 << CS12) | (1 << WGM12) | (1 << WGM13);
}

void init_interrupts(void)
{
    timer_init(F_CPU/256 - 1);
    sei();
}

void Timer1_ISR(void)
{
    SW_TIMER_FLAG = 1;
    seconds++;
    TIFR1 = (0 << OCF1A);
}

uint16_t getSeconds(void)
{
    return seconds;
}

int getTimerFlag(void)
{
    return SW_TIMER_FLAG;
}

void resetTimerFlag(void)
{
    SW_TIMER_FLAG = 0;
}

ISR(TIMER1_COMPA_vect)
{
    Timer1_ISR();
}