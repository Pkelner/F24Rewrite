#ifndef _UART_H
#define _UART_H
#include <stdarg.h>
#include <compiler.h>
#ifdef __cplusplus
extern "C" {
#endif

void UART_Initialize(uint32_t baud_rate);

bool UART_Receive(uint8_t *read_char);

void UART_Transmit(uint8_t data);

void UART_printf(const char * outStr);

bool
UART_putbyte(
    uint8_t c
    );

int uprintf(const char* s, ...);

#ifdef __cplusplus
}
#endif
#endif

