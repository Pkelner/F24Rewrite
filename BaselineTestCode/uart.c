/*
 * uart.c
 *
 */ 

#include <avr/io.h>
#include <avr/iocanxx.h>
#include <compiler.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ringbuffer.h"
#include "uart.h"

#define OSCSPEED    F_CPU        /* in Hz */
#define MAX_TX_SIZE (128)

RING_BUFFER txBuffer;
uint8_t txBufferBacking[MAX_TX_SIZE];

void PORT_Init(void);
void UART_Initialize(uint32_t baud_rate);
bool UART_Receive(uint8_t *read_char);
void UART_Transmit(uint8_t data);
void UART_Interrupts_Enable(void);
void UART_Interrupts_Disable(void);


void PORT_Init(void)
{
    PORTA |= 0x00;
    DDRA  |= 0x00;
    
    PORTB |= 0x00;
    DDRB  |= 0x00;
    
    PORTC |= 0x00;
    DDRC  |= 0x00;
    
    PORTD |= 0x00;
    DDRD  |= 0x00;
    
    /* No internal pull-up used (PE0=0) */
    PORTE |= 0x00;
    
    /* UART0 Transmit pin (PE1=1) and UART0 Receive pin (PE0=0) */
    DDRE   = (DDRE & ~((1 << PE1) | (1 << PE0))) | (1 << PE1);
    
    PORTF |= 0x00;
    DDRF  |= 0x00;
}

void UART_Initialize(uint32_t baud_rate)
{
    unsigned int BaudRate = OSCSPEED / (16 * baud_rate) - 1; /* as per pg. 173 of the user manual */

    InitializeRingBuffer(&txBuffer, txBufferBacking, MAX_TX_SIZE);
    
    PORT_Init();
    
    //set BaudRate to registers UBRR1H and UBRR1L
    UBRR0H = (unsigned char) ((BaudRate & 0xFF00) >> 8);
    UBRR0L = (unsigned char) (BaudRate & 0xFF);

    /*
     * Enable Receiver and Transmitter
     */
    UCSR0B = (1 << RXEN0) | (1 << TXEN0);

    /*
     * Set frame format: 
     *      asynchronous operation (Bit6 = 0),
     *      disabled parity (Bit4 = 0, Bit5 = 0),
     *      1 stop bit (Bit3 = 0),
     *      8 data (Bit1 = 1, Bit2 = 1),
     *      Rising Edge Polarity (Bit0 = 0)
     */
    UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);
}

void UART_Interrupts_Enable(void)
{
    UCSR0B |= (1 << UDRIE0);
}

void UART_Interrupts_Disable(void)
{
    UCSR0B &= ~(1 << UDRIE0);
}

bool UART_Receive(uint8_t *read_char)
{
    /* if there is unreaded data */
    if (UCSR0A & (1 << RXC))
    {
        *read_char = UDR0;

        return true;
    }
    else
    {
        return false;
    }
}

void UART_Transmit(uint8_t data)
{
    /* 
     * waiting until buffer is ready to receive
     */
    
    while (!(UCSR0A & (1 << UDRE0)));
    
    UDR0 = data;
}

bool UART_putbyte(uint8_t c)
{
    bool success = InterlockedWriteRingBuffer(&txBuffer, &c, 1);
    
    if(success)
    {
        UART_Interrupts_Enable();
    }
    
    return success;
}

void UART_printf(const char * outStr)
{
    int index;
    
    for (index = 0; index <= strlen(outStr); index++)
    {
        UART_Transmit(outStr[index]);
    }
}    

ISR(USART0_UDRE_vect)
{
    uint8_t bytes_left;
    uint8_t byte_out;
    
    UART_Interrupts_Disable();
    bytes_left = BytesReadyRingBuffer(&txBuffer);
    
    if (bytes_left > 0)
    {
        InterlockedReadRingBuffer(&txBuffer, &byte_out, 1);
        UART_Transmit(byte_out);
        UART_Interrupts_Enable();
    }
    else
    {
        UART_Interrupts_Disable();
    }
}